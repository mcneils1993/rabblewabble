//
//  QuestionStrategy.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-13.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

public protocol QuestionStrategy: class {
    //Will be the title for which set of question is selected such as "Basic Phrases"
    var title: String { get }
    
    //these two variable will return the current number of correct and incorrect questions
    var correctCount: Int { get }
    var incorrectCount: Int { get }
    
    //Will be used to advanced to the next question, if there isn't one available then return false
    func advanceToNextQuestion() -> Bool
    
    //Simply return the current question.
    func currentQuestion() -> Question
    
    //Will mark a question correct and mark a question incorrect
    func markQuestionCorrect(_ question: Question)
    func markQuestionIncorrect(_ question: Question)
    
    //Will return the index title for the current question to indicate progress such as " 1 / 10" for the first question out of 10
    func questionIndexTitle() -> String
}
