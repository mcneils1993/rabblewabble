//
//  RandomQuestionStrategy.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-14.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation
import GameplayKit.GKRandomSource

class RandomQuestionStrategy: BaseQuestionStrategy {
   
    public convenience init(questionGroupCaretaker: QuestionGroupCaretaker) {
        let questionGroup = questionGroupCaretaker.selectedQuestionGroup!
        let randomSource = GKRandomSource.sharedRandom()
        let questions = randomSource.arrayByShufflingObjects(in: questionGroup.questions) as! [Question]
        
        self.init(questionGroupCareTaker: questionGroupCaretaker, questions: questions)
    }
}
