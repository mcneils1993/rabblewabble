//
//  SequentialQuestionStrategy.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-13.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

//    Super                       Base
class SequentialQuestionStrategy: BaseQuestionStrategy {
    
    public convenience init(questionGroupCaretaker: QuestionGroupCaretaker) {
        let questionGroup = questionGroupCaretaker.selectedQuestionGroup!
        let questions = questionGroup.questions
        self.init(questionGroupCareTaker: questionGroupCaretaker, questions: questions)
    }
}
