//
//  BaseQuestionStrategy.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-06-02.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

class BaseQuestionStrategy: QuestionStrategy {
    
    //MARK:- Properties
    private var questionGroupCareTaker: QuestionGroupCaretaker
    private var questionGroup: QuestionGroup {
        return questionGroupCareTaker.selectedQuestionGroup
    }
    private var questionIndex = 0
    private let questions: [Question]
    
    public var correctCount: Int {
        get { return questionGroup.score.correctCount }
        set { questionGroup.score.correctCount = newValue }
    }
    
    public var incorrectCount: Int {
        get { return questionGroup.score.incorrectCount }
        set { questionGroup.score.incorrectCount = newValue }
    }
    //MARK:- Object Lifecycle
    //3
    public init(questionGroupCareTaker: QuestionGroupCaretaker, questions: [Question]) {
        self.questionGroupCareTaker = questionGroupCareTaker
        self.questions = questions
        
        //4
        self.questionGroupCareTaker.selectedQuestionGroup.score.reset()
    }
    
    //MARK:- QuestionStrategy
    public var title: String {
        return questionGroup.title
    }
    
    public func currentQuestion() -> Question {
        return questions[questionIndex]
    }
    
    public func advanceToNextQuestion() -> Bool {
        try? questionGroupCareTaker.save()
        guard questionIndex + 1 < questions.count else { return false }
        questionIndex += 1
        return true
    }
    
    public func markQuestionCorrect(_ question: Question) {
        correctCount += 1
    }
    
    public func markQuestionIncorrect(_ question: Question) {
        incorrectCount += 1
    }
    
    public func questionIndexTitle() -> String {
        return "\(questionIndex + 1)/\(questions.count)"
    }
}
