//
//  QuestionGroupCaretaker.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-27.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

//Declare a new class called QuestionGroupCaretaker. You'll use this to save and retrieve QuestionGroup objects by calling the necessary static methods in DiskCaretaker.
public final class QuestionGroupCaretaker {
    
    //MARK:- Properties
    /* We declare three propertiers:
        - The file where you'll save and retrieve QuestionGroup objects
        - questionGroups, will hold onto the question that are in use
        - selectedQuestionGroup, will hold onto whichevers selection the user makes */
    private let fileName = "QuestionGroupData"
    public var questionGroups: [QuestionGroup] = []
    public var selectedQuestionGroup: QuestionGroup!
    
    //MARK:- Object Lifecylces
    public init() {
        loadQuestionGroups()
    }
    
    /* You perform the retrieve actions here. First you attempt to load the QuestionGroups from the user's Documents directore using fileName. If the file hasn't been created,
       this will fail and return nil and jump into the else. In the case of failure we load the QuestionGroups from Bundle.main and then call save() to write this file to the user's
       Documents directory. */
    private func loadQuestionGroups() {
        if let questionGroups = try? DiskCaretaker.retrieve([QuestionGroup].self, from: fileName) {
            self.questionGroups = questionGroups
        } else {
            let bundle = Bundle.main
            let url = bundle.url(forResource: fileName, withExtension: "json")!
            self.questionGroups = try! DiskCaretaker.loadURLData([QuestionGroup].self, from: url)
            try! save()
        }
    }
    
    //Write the file to the users Documents Directory
    public func save() throws {
        try DiskCaretaker.save(questionGroups, to: fileName)
    }
}
