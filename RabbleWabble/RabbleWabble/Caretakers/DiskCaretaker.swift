//
//  DiskCaretaker.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-22.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

public final class DiskCaretaker {
    public static let decoder = JSONDecoder()
    public static let encoder = JSONEncoder()
    
    /* We use this method to save Codable objects, we first delcare a generic method that takes any object that conforms to Codable */
    public static func save<T: Codable>(_ object: T, to fileName: String) throws {
        do {
            /* Then we call createDocumentURL to create a document URL for the given file name. */
            let url = createDocumentURL(withFileName: fileName)
            encoder.outputFormatting = .prettyPrinted
            /* You use encoder to encode the object into data. This operation may throw an error*/
            let data = try encoder.encode(object)
            
            /* You call data.write to write the data to the given url. You use the atomic operator to instruct iOS to create a temporate file and then move it to the desired path.
               This has a small performence cost but ensures the file will never be corrupted. */
            try data.write(to: url, options:. atomic)
        } catch {
            /* You catch an error, your print the object and error to the console then throw the error. */
            print("Save faile: Object `\(object)`, " + "Error: `\(error)`")
            throw error
        }
    }
    
    /* We can declare a method for retrieving objects given a type and fileName. This method first create a file URL and call retrieve(_:from). */
    public static func retrieve<T: Codable>(_ type: T.Type, from fileName: String) throws -> T {
        let url = createDocumentURL(withFileName: fileName)
        return try loadURLData(T.self, from: url)
    }
    
    /* You declare a method which takes a URL rather than a String, which does the actual loading. The previous methods calls through this one. */
    public static func loadURLData<T: Codable>(_ type: T.Type, from url: URL) throws -> T {
        do {

            /* Here we create a data instance from the given file url */
            let data = try Data(contentsOf: url)
            /* You then use a decoder to decode the object in data. */
//            let printData = String(data: data, encoding: .utf8)
//            print(printData!)
            return try decoder.decode(T.self, from: data)
        } catch {
            print("Retrieve failed: URL `\(url), Error: `\(error)`")
            throw error
        }
    }
    
    public static func createDocumentURL(withFileName fileName: String) -> URL {
        let fileManager = FileManager.default
        let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        return url.appendingPathComponent(fileName).appendingPathExtension("json")
    }
}
