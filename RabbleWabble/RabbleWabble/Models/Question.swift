//
//  Question.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-06.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

public class Question: Codable {
    public let answer: String
    public let hint: String?
    public let prompt: String
    
    public init(answer: String, hint: String?, prompt: String) {
        self.answer = answer
        self.hint = hint
        self.prompt = prompt
    }
}
