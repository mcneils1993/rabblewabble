//
//  AppSettings.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-14.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation


public enum QuestionStrategyType: Int, CaseIterable {
    case random
    case sequential
    
    //MARK:- Instance Methods
    var title: String {
        switch self {
        case .random:
            return "Random"
        case .sequential:
            return "Sequential"
        }
    }
    
    public func questionStrategyConvenience(for questionGroupCaretaker: QuestionGroupCaretaker) -> QuestionStrategy {
        switch self {
        case .random:
            return RandomQuestionStrategy(questionGroupCaretaker: questionGroupCaretaker)
        case .sequential:
            return SequentialQuestionStrategy(questionGroupCaretaker: questionGroupCaretaker)
        }
    }
}

public class AppSettings {
    
    //MARK:- Keys
    public struct Keys {
        static let questionStrategy = "questionStrategy"
    }
    
    //MARK:- Static Properties
    public static let shared = AppSettings()

    //MARK:- Instance Properties
    private let userDefaults = UserDefaults.standard

    /* We use this variable to hold onto the user's desired strategy. Instead of just a simple property we oveerride the getter and setter
       to get and set the integer value using UserDefaults. We use userDefaults to store key-value pairs that persist across app launches.
     
       If the specified key doesn't exist yet the method will return 0 setting the default selected row: print(userDefaults.integer(forKey: Keys.questionStrategy))
     */
    public var questionStrategyType: QuestionStrategyType {
        get {
            let rawValue = userDefaults.integer(forKey: Keys.questionStrategy)
            return QuestionStrategyType(rawValue: rawValue)!
        } set {
            userDefaults.set(newValue.rawValue, forKey: Keys.questionStrategy)
        }
    }

    
    //MARK:- Object Lifecycle
    /* The class init() is marked with private because the private keyword ensures that the AppSettings class can only be initalized within the AppSettings.
       In other words, we cannot create an instance of AppSettings outside of the AppSettings class. This ensures that AppSettings object we've created is the only instance
       in our code.
     */
    private init() {}
    
    //MARK:- Instance Methods
    /* Convenience method to get the QuestionStrategy from the selected questionStrategyType */
    public func questionStrategy(for questionGroupCareTaker: QuestionGroupCaretaker) -> QuestionStrategy {
        return questionStrategyType.questionStrategyConvenience(for: questionGroupCareTaker)
    }
}
