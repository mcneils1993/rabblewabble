//
//  QuestionView.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-06.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

public class QuestionView: UIView {
    @IBOutlet public var answerLabel: UILabel!
    @IBOutlet public var correctCountLabel: UILabel!
    @IBOutlet public var incorrectCountLabel: UILabel!
    @IBOutlet public var promptLabel: UILabel!
    @IBOutlet public var hintLabel: UILabel!

}
