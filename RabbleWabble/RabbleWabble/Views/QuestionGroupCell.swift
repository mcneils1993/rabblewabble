//
//  QuestionGroupCell.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-07.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit
import Combine

public class QuestionGroupCell: UITableViewCell {
    @IBOutlet public var titleLabel: UILabel!
    @IBOutlet public var percentageLabel: UILabel!
    
    public var percentageSubscriber: AnyCancellable?
}
