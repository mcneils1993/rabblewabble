//
//  QuestionGroupBuilder.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-06-12.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

public class QuestionGroupBuilder {
    
    public enum Error: String, Swift.Error {
        case missingTitle
        case missingQuestions
    }
    
    /* We first declare properties maching the required inputs to create a QuestionGroup. You create an array of QuestionBuilders, which will build the
       the individual question objects. You initially create a single QuestionBuilder so that there is one to start with because a QuestionGroup must have one question. */
    public var questions = [QuestionBuilder]()
    public var title = ""

    
    public func addNewQuestion() {
        let question = QuestionBuilder()
        questions.append(question)
    }

    public func removeQuestion(at index: Int) {
        questions.remove(at: index)
    }
    
    /* Whenever we call buildQuestionGroup(), the QuestionGroupBuilder validates the title and makes sure there's at least one QuestionBuilder in the questions.
       It then maps the question array that is of type QuestionBuilder and calls buildQuestion() for each instance.
     
       Once we're finish mapping and have our new array of questions then we create are QuestionGroup by passing questions and title as the arguments.
     */
    public func buildQuestionGroup() throws -> QuestionGroup {
        guard self.title.count > 0 else { throw Error.missingTitle }
        guard self.questions.count > 0 else { throw Error.missingQuestions }
        
        let questions = try self.questions.map { try $0.buildQuestion() }
        return QuestionGroup(questions: questions, title: title)
    }
}
