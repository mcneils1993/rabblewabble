//
//  QuestionBuilder.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-06-12.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

public class QuestionBuilder {
    
    public enum Error: String, Swift.Error {
        case missingAnswer
        case missingPrompt
    }
    
    public var answer = ""
    public var hint = ""
    public var prompt = ""
    
    public func buildQuestion() throws -> Question {
        guard answer.count > 0 else { throw Error.missingAnswer }
        guard prompt.count > 0 else { throw Error.missingPrompt }
        return Question(answer: answer, hint: hint, prompt: prompt)
    }
}
