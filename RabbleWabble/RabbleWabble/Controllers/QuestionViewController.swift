//
//  ViewController.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-06.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

public protocol QuestionViewControllerDelegate: class {
    //Call when the uses presses the cancel button
    func questionViewController(_ viewController: QuestionViewController, didCancel questionGroup: QuestionStrategy)
    
    //Call when the user completes all of the questions
    func questionViewController()
}

//public means anything is publicly accessible to the type itself. 
public class QuestionViewController: UIViewController {
    
    //MARK: - Instance Properties
    public var questionStrategy: QuestionStrategy! {
        didSet {
            navigationItem.title = questionStrategy.title
        }
    }
    
    public weak var delegate: QuestionViewControllerDelegate?

    //creating a custom bar button item to display in the navigation bar to show the remaining number of questions in a question group.
    private lazy var questionIndexItem: UIBarButtonItem = {
        let item = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        item.tintColor = .black
        navigationItem.rightBarButtonItem = item
        return item
    }()
    
    public var questionIndex = 0 //index of current question displayed
    public var correctCount = 0
    public var incorrectCount = 0
    
    /* This is a computer property. Here you check if the view is loaded so we won't cause the view to loaded unintentionnally by accessing this property.
       If the view is already loaded, you force cast it to QuestionView */
    public var questionView: QuestionView! {
        guard isViewLoaded else { return nil }
        return (view as! QuestionView)
    }
    
    //MARK: - View Lifecycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupCancelButton()
        showQuestions()
    }
    
    /* Here we create a function in the controller to manipulate the views based on the data in the models. */
    private func showQuestions() {
        let question = questionStrategy.currentQuestion()
        
        questionView.answerLabel.text = question.answer
        questionView.promptLabel.text = question.prompt
        questionView.hintLabel.text = question.hint
        
        questionView.answerLabel.isHidden = true
        questionView.hintLabel.isHidden = true
        
        questionIndexItem.title = questionStrategy.questionIndexTitle()
    }
    
    private func showNextQuestion() {
        guard questionStrategy.advanceToNextQuestion() else {
            delegate?.questionViewController()
            return
        }
        showQuestions()
    }
    
    private func setupCancelButton() {
        let action = #selector(handleCancelPressed(sender:))
        let image = UIImage(named: "ic_menu")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, landscapeImagePhone: nil, style: .plain, target: self, action: action)
    }
    
    @objc func handleCancelPressed(sender: UIBarButtonItem) {
        delegate?.questionViewController(self, didCancel: questionStrategy)
    }
    
    //MARK: - Actions
    
    /* All these examples below are telling the view to notify the controller about an action that has happened.
       In response, the controller executes code for handling the action. */
    @IBAction func toggleAnswerLabels(_sender : Any) {
        questionView.answerLabel.isHidden = !questionView.answerLabel.isHidden
        questionView.hintLabel.isHidden = !questionView.hintLabel.isHidden
    }
    
    @IBAction func handleCorrect(_ sender: Any) {
        let question = questionStrategy.currentQuestion()
        questionStrategy.markQuestionCorrect(question)
    
        questionView.correctCountLabel.text = String(questionStrategy.correctCount)
        showNextQuestion()
    }
    
    @IBAction func handleIncorrect(_ sender: Any) {
        let question = questionStrategy.currentQuestion()
        questionStrategy.markQuestionIncorrect(question)

        questionView.incorrectCountLabel.text = String(questionStrategy.incorrectCount)
        showNextQuestion()
    }
}

