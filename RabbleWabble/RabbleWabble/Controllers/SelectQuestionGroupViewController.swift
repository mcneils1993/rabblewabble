//
//  SelectQuestionGroupViewController.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-07.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

//We will use this view controller to display a list of quetion groups.
public class SelectionQuestionViewController: UIViewController {
    
    /* Whenever the tableView is set, you set tableview.tableFooterView to a blank UIView. This trick is to prevet the table view from drawing
       any unnecessary empty table view cells, which it does by default after all the other cells are drawn. */
    @IBOutlet internal var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
        }
    }
    
    //MARK:- Properties
    private let questionGroupCaretaker = QuestionGroupCaretaker()
    private var questionGroups: [QuestionGroup] {
        return questionGroupCaretaker.questionGroups
    }
    private var selectedQuestionGroup: QuestionGroup! {
        get {
            return questionGroupCaretaker.selectedQuestionGroup
            
        } set {
            questionGroupCaretaker.selectedQuestionGroup = newValue
        }
    }
    
    private let appSettings = AppSettings.shared
    
    //MARK:-Lifecycle
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        questionGroups.forEach {
            print("\($0.title): " + "correctCount: \($0.score.correctCount), " + "incorrectCount: \($0.score.incorrectCount)")
        }
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /* There's another segue that is possible, which shows the QuestionViewController. Previously, this was the only code within this method.
           You check if this is the case, and if so, set the propertieson QuestionViewController correctly. */
        if let viewController = segue.destination as? QuestionViewController {
            viewController.delegate = self
            viewController.questionStrategy = appSettings.questionStrategy(for: questionGroupCaretaker)
            
            /* You check if the segue is transitioning to a CreateQuestionGroupViewController within a UINavigationController. If so, you set the delegate
               on the new CreateQuestionGroupViewController instance. */
        } else if let navController = segue.destination as? UINavigationController,
            let viewController = navController.topViewController as? CreateQuestionGroupViewController {
            viewController.delegate = self
        }
    }
}

extension SelectionQuestionViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionGroups.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionGroupCell") as! QuestionGroupCell
        let questionGroup = questionGroups[indexPath.row]
        cell.titleLabel.text = questionGroup.title
        
        cell.percentageSubscriber = questionGroup.score.$runningPercentage.receive(on: DispatchQueue.main).map() {
            return String(format: "%0.f %%", round(100 * $0))
        }.assign(to: \.text, on: cell.percentageLabel)
        return cell
    }
}

extension SelectionQuestionViewController: UITableViewDelegate {
    
    //We do this here instead of didSelectRowAt because didSelectRowAt is called after the segue is performed.
    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        //this sets the questionGroupCaretaker.selectedQuestionGroup inside the computer property of selectedQuestionGroup
        selectedQuestionGroup = questionGroups[indexPath.row]
        return indexPath
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SelectionQuestionViewController: QuestionViewControllerDelegate {

    /* For now we simply pop to the SelectQuestionViewController regardless of which delegate method is called */

    public func questionViewController(_ viewController: QuestionViewController, didCancel questionGroup: QuestionStrategy) {
        navigationController?.popToViewController(self, animated: true)
    }

    public func questionViewController() {
        navigationController?.popToViewController(self, animated: true)
    }
}

//MARK:- CreateQuestionGroupViewControllerDelegate
extension SelectionQuestionViewController: CreateQuestionGroupViewControllerDelegate {
    public func createQuestionGroupViewControllerDidCancel(_ viewController: CreateQuestionGroupViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    public func createQuestionGroupViewController(_ viewController: CreateQuestionGroupViewController, created questionGroup: QuestionGroup) {
        questionGroupCaretaker.questionGroups.append(questionGroup)
        try? questionGroupCaretaker.save()
        
        dismiss(animated: true, completion: nil)
        tableView.reloadData()
    }
}
