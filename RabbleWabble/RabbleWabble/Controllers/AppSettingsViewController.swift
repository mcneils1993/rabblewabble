//
//  AppSettingsViewController.swift
//  RabbleWabble
//
//  Created by Simon McNeil on 2020-05-16.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation
import UIKit

public class AppSettingsViewController: UITableViewController {
    
    //MARK:- Properties
    private let appSettings = AppSettings.shared
    private let cellIdentifier = "basicCell"
    
    //MARK:- View Lifecylce
    public override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        /* if we don't create a custom uitableview cell just use this and delete the prototype cell in your tableView storyboard */
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    //MARK:- UITableViewDelegate
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //The allCases is why we made the enum CaseIterable. Note make sure you type allCases (won't show in auto complete) and not AllCases
        return QuestionStrategyType.allCases.count
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        let questionStrategyType = QuestionStrategyType.allCases[indexPath.row] //gets value from the enum
        
        cell.textLabel?.text = questionStrategyType.title
        
        /* If we appSettings.questionStrategyType is equal to the given questionStrategyType, it's the currently selected strategy.
           Which you check mark. */
        if appSettings.questionStrategyType == questionStrategyType {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    //MARK:- UITableViewDataSource
    /* Whenever a cell is selected, you get the questionStrategyType for the given cells indexPath.row, set this as appSettings.questionStrategyType
       and reload the tableView. */
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let questionStrategyType = QuestionStrategyType.allCases[indexPath.row]
        appSettings.questionStrategyType = questionStrategyType
        tableView.reloadData()
    }
}
