
import UIKit

public protocol CreateQuestionGroupViewControllerDelegate {

  func createQuestionGroupViewControllerDidCancel(_ viewController: CreateQuestionGroupViewController)

  func createQuestionGroupViewController(_ viewController: CreateQuestionGroupViewController,
                                         created questionGroup: QuestionGroup)
}

public class CreateQuestionGroupViewController: UITableViewController {

    // MARK: - Properties
    public var delegate: CreateQuestionGroupViewControllerDelegate?
    public let questionGroupBuilder = QuestionGroupBuilder()

    public override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

  // MARK: - IBActions
  @IBAction func cancelPressed(_ sender: Any) {
    delegate?.createQuestionGroupViewControllerDidCancel(self)
  }

    @IBAction func savePressed(_ sender: Any) {
        do {
            let questionGroup = try questionGroupBuilder.buildQuestionGroup()
            delegate?.createQuestionGroupViewController(self, created: questionGroup)
        } catch {
            displayMissingInputsAlert()
        }
    }
    
    public func displayMissingInputsAlert() {
        let alert = UIAlertController(title: "Missing Inputs", message: "Please Provide all non-optional inputs", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension CreateQuestionGroupViewController {

  fileprivate struct CellIdentifiers {
    fileprivate static let add = "AddQuestionCell"
    fileprivate static let title = "CreateQuestionGroupTitleCell"
    fileprivate static let question = "CreateQuestionCell"
  }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /* CreateQuestionGroupViewController displays three types of cells.
         
            1. One of the title for the QuestionGroup,
            2. One for each QuestionBuilder
            3. One to add additional QuestionBuilder objects */
        print(questionGroupBuilder.questions.count + 2)
        return questionGroupBuilder.questions.count + 2
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        if row == 0 {
            return titleCell(from: tableView, for: indexPath)
        } else if row >= 1 && row <= questionGroupBuilder.questions.count {
            return self.questionCell(from: tableView, for: indexPath)
        } else {
            return addQuestionGroupCell(from: tableView, for: indexPath)
        }
    }

    //Creates the first cell that we see, the one where we will give our new question group a title.
    private func titleCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.title, for: indexPath) as! CreateQuestionGroupTitleCell
        cell.delegate = self
        cell.titleTextField.text = questionGroupBuilder.title
        return cell
    }

    //This creates the new group of cells questions cells (Answer, Hint, Question)
    private func  questionCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.question, for: indexPath) as! CreateQuestionCell
        let questionBuilder = self.questionBuilder(for: indexPath)

        cell.delegate = self
        cell.indexLabel.text = "Questions \(indexPath.row)"
        cell.promptTextField.text = questionBuilder.prompt
        cell.hintTextField.text = questionBuilder.hint
        cell.answerTextField.text = questionBuilder.answer

        return cell
    }
    
    //Creates the cell to display the green add button
    private func addQuestionGroupCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.add, for: indexPath)
    }
    
    //This is a helper method to get the QuestionBuilder for a given index path.
    private func questionBuilder(for indexPath: IndexPath) -> QuestionBuilder {
        print(indexPath.row)
        return questionGroupBuilder.questions[indexPath.row - 1]
    }
}

// MARK: - UITableViewDelegate
extension CreateQuestionGroupViewController {

    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard isLastIndexPath(indexPath) else { return }
        questionGroupBuilder.addNewQuestion()
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    private func isLastIndexPath(_ indexPath: IndexPath) -> Bool {
        /* Whenever a table view cell is tapped in didSelectRowAt (method above) we call this function to see if selected the "Add" cell at the bottom of the table view.
           In this case we request questionGroupBuilder.addNewQuestion() and insert a new cell to show the next QuestionBuilder. */
        print("\(indexPath.row)")
        print("\(indexPath.section)")
        print("\(tableView.numberOfRows(inSection: indexPath.section))")
        return indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1
    }
}

// MARK: - CreateQuestionCellDelegate
extension CreateQuestionGroupViewController: CreateQuestionCellDelegate {

    public func createQuestionCell(_ cell: CreateQuestionCell, answerTextDidChange text: String) {
        getQuestionBuilderInstance(for: cell).answer = text
    }

    public func createQuestionCell(_ cell: CreateQuestionCell, hintTextDidChange text: String) {
        getQuestionBuilderInstance(for: cell).hint = text
    }

    public func createQuestionCell(_ cell: CreateQuestionCell, promptTextDidChange text: String) {
        getQuestionBuilderInstance(for: cell).prompt = text
    }
    
    /* We use this helper to determine the QuestionBuilder for a given cell, which we do so by finding the cell's indexPath and then using the helper
       method we wrote eariler. */
    private func getQuestionBuilderInstance(for cell: CreateQuestionCell) -> QuestionBuilder {
        let indexPath = tableView.indexPath(for: cell)!
        return questionBuilder(for: indexPath)
    }
}

// MARK: - CreateQuestionGroupTitleCellDelegate
extension CreateQuestionGroupViewController: CreateQuestionGroupTitleCellDelegate {

    public func createQuestionGroupTitleCell(_ cell: CreateQuestionGroupTitleCell, titleTextDidChange text: String) {
        questionGroupBuilder.title = text
    }
}
